#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
from time import sleep


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.
NAO_IP = "192.168.181.201"
NAO_PORT = 8080
MOTOR_SPEED = 250 # Unit in RPM
MOTOR_MAX_TORQUE = 50 # Unit in %
MOTOR_DegPerSec = MOTOR_SPEED * 10 # Unit in degrees / second
FLAG_FLIP_deg = -170 # Unit in degrees
POSSIBLE_COLORS = [ Color.RED, Color.YELLOW, Color.BLUE, Color.WHITE ] # Verde non è Color.GREEN ma WHITE

# Create your objects here.
ev3 = EV3Brick()

# Write your program here.
def sendNaoCommand(cmd):
    from socket import AF_INET, SOCK_STREAM, socket as Socket
    socket = Socket(AF_INET, SOCK_STREAM)
    try:
        socket.settimeout(1)
        socket.connect((NAO_IP, NAO_PORT))
        socket.send(cmd)
        socket.settimeout(None)
        socket.close()
    except Exception as e:
        ev3.screen.print("NAO command")
        ev3.screen.print("error:")
        ev3.screen.print(str(cmd))
        print("Could not send command '%s' to %s:%d for error:" % (str(cmd), NAO_IP, NAO_PORT))
        print(str(e))
        

def waitForBrickButton(button):
    while Button.CENTER not in ev3.buttons.pressed():
        pass

def waitForSomeColor(colorSensor):
    color = colorSensor.color()
    while color not in POSSIBLE_COLORS:
        color = colorSensor.color()
        #print(color, "!=", POSSIBLE_COLORS)
    #if color != None:
    print(color, "!=", POSSIBLE_COLORS)
    return color
        

def resetMotor(motor):
    MOTOR_MAX_TORQUE = 10 # Unit in %
    motor.run_until_stalled(MOTOR_DegPerSec, then=Stop.HOLD, duty_limit=MOTOR_MAX_TORQUE)
    motor.reset_angle(0)

def flipFlag(motor, perTempoSecondi):
    motor.run_angle(MOTOR_DegPerSec, FLAG_FLIP_deg)
    sleep(perTempoSecondi)
    motor.run_angle(MOTOR_DegPerSec, -FLAG_FLIP_deg)

ev3.speaker.beep()

motorA = Motor(Port.A)
motorB = Motor(Port.B)
motorC = Motor(Port.C)
motorD = Motor(Port.D)
colorSensor4 = ColorSensor(Port.S2)

waitForBrickButton(Button.CENTER)
resetMotor(motorA)
resetMotor(motorB)
resetMotor(motorC)
resetMotor(motorD)

while Button.CENTER not in ev3.buttons.pressed():
    ev3.speaker.beep()
    color = waitForSomeColor(colorSensor4)
    #color = colorSensor4.color()
    if color == Color.RED:
        sendNaoCommand("Flag1") # Cinese
        flipFlag(motorA, 4)
    if color == Color.YELLOW:
        sendNaoCommand("Flag2") # Japonese
        flipFlag(motorB, 4)
    if color == Color.BLUE:
        sendNaoCommand("Flag3") # Inglese
        flipFlag(motorC, 4)
    if color == Color.WHITE: # Verde non è Color.GREEN ma WHITE
        sendNaoCommand("Flag4") # Italiano
        flipFlag(motorD, 7)

ev3.speaker.beep()
